//
//  AppDefaults.swift
//  ZibdyHealth
//
//  Created by Mitali Desai on 22/9/21.
//

import Foundation

//Wrapper struct to get and store user defaults store in the app from predefined keys
struct AppDefaults {
    
    //Update the cases as per need, make sure to have key in a non-human readable form
    enum Key : String{
        
        //User Related Details
        case UserType = "sxdcfvgb"
        case RecordeVideoLink = "djjfashfgsahgdfskbkbsbfds"
        case SaveScript = "ifgskdbffsbdfkbrigfigdbvsg"
        case CueIndicatoreYPosition = "gbabfdudbfdsbfdsbfabfdbbc"
        case isShowCueIndicator = "bdsfjkagjgfdbjuydjdgfjk"
        case isHideTopBarwhileScrolling = "kdfljhskbksjdbkjbvsjkd"
        case isShowCountdown = "dbfkgigfgsfirgigfiu"
        case isVideoPlayIn4K = "dfgsgfisahcwgritrgfbgdkgfdgfoaf"
        case isShowProgressBar = "djsfglsjdfhsdvfvdsuydhvl"
        case isFontChangeInUpperCase = "dkhvfjsvkdvfljagsfdluywvdjhv"
        case TapToScrollEnable = "dnfksgdfiyfgdhjsbdjfgdsuyg"
        case isOnSyncScrolling = "dffkaghbcfuybrgw"
        case changeFontFamily = "dfgdgfksdgdfg"
        case isMirrorTextHorizontally = "fghgfsdgfhgoigfihdfhgfjdsbfufu"
        case isMirrorTextVertically = "dfdusfgfgdsfgfdugdsg"
        case SetMarginsLed = "dkgsdkjfhfjhbv"
        case SetMarginsTri = "dnfkjsaflsflkfsgh"
        case arrSaveURL = "dfghfjsdkgsdhgiytfrbdcvbjhcge"
        case isSubscribed = "kdgh;dsfhgpfdghjvjhfddjggg"
        case isSeenOnBoarding = "ghfdgfhhjgflhgfl"
    }
    
    private init(){} // Making the struct single ton
    
    ///Store a value on key from the enum list
    /// - parameter value : value you want to store in `UserDefaults`
    /// - parameter key : key where you want to store value in `UserDefaults`
    static func store(_ value : Any, for key : Key){
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }
    
    ///return a value of the key from the enum list
    /// - parameter key : key of which you want to fetch value from `UserDefaults`
    static func get(_ key : Key) -> Any?{
        return UserDefaults.standard.object(forKey: key.rawValue)
    }
    
    ///store a codable model object to User defaults
    /// - parameter model : Codable model type of the object
    /// - parameter key : Specify key to store data
    /// - note : This func will convert the object into data and stores it in Userdefauls
    /// - Note : Use Realm for large data instead of this functions
    /// func will throw the error if failes to encode the object
    static func store<T : Encodable>(encode model : T, for key : Key) throws {
        
        let encoder = JSONEncoder()
        let encodedData = try encoder.encode(model)
        UserDefaults.standard.set(encodedData, forKey: key.rawValue)
        
    }
    
    ///Will get the stored object from Userdefaults and return the decoded form
    /// - parameter model : Codable model to use for decoding
    /// - parameter key : key where data is stored
    /// - Note : Use Realm for large data instead of this functions
    /// - it will retrive data from specified key and returns the decodeed object of given model
    static func get<T>(docode model : T.Type, for key : Key) throws  -> T? where T : Decodable{
        
        let decoder = JSONDecoder()
        if let encodedData = UserDefaults.standard.data(forKey: key.rawValue){
            
            let decodedData = try decoder.decode(model, from: encodedData)

            return decodedData
            
        }else{
            return nil
        }
        
    }
    
    ///Will remove the data of specified key
    static func remove(key : Key){
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
    
}
