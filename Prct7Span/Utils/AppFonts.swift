//
//  Font.swift
//  Prct7Span
//
//  Created by Mitali Desai on 1/04/2023.
//

import Foundation
import SwiftUI


///App Fonts class have default fonts used system wide
struct AppFonts : ViewModifier{
    
    ///Name of the font from available list
    var fontName : Font.Weight
    
    ///Size of the font
    var size : CGFloat
    
    ///Color for the font
    var forgroundColor : Color
    
    ///Modifier will apply the font to any generic view
    func body(content: Content) -> some View {
        content
            .font(.system(size: size,weight: fontName))
            .foregroundColor(forgroundColor)
            
    }
}
