//
//  Typealias.swift
//  Prct7Span
//
//  Created by Mitali Desai on 26/03/23.
//

import Foundation

typealias BindFail = ((_ message : String ,_ status : Bool)->())
typealias emptyCompletion = () -> Void
