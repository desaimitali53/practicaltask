//
//  NavigationHeaderView.swift
//  Prct7Span
//
//  Created by Mitali Desai on 01/04/23.
//

import Foundation
import SwiftUI

struct NavigationHeaderView: View {
    
    @Environment(\.presentationMode) var PresentationMode
    var isTitleShow : Bool? = true
    var Title : String? = ""
    var isShowBack : Bool? = false
    var body: some View {
        HStack{
            
            ZStack{
                if isShowBack ?? false{
                    HStack{
                        
                        Button {
                            PresentationMode.wrappedValue.dismiss()
                        } label: {
                            Image("back")
                                .resizable()
                                .frame(width: 10, height: 20)
                                .aspectRatio(contentMode: .fit)
                        }.padding(.leading)
                        
                        Spacer()
                    }
                }
                HStack{
                    
                    Spacer()
                    
                        Text(Title ?? "Title")
                        .modifier(AppFonts(fontName: .bold, size: 16, forgroundColor: .black))
                   
                    Spacer()
                }
            }
        }
    }
}

struct navigationWithLogo_Previews: PreviewProvider {
    static var previews: some View {
        NavigationHeaderView()
    }
}

