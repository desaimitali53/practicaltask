//
//  View+Extension.swift
//  Prct7Span
//
//  Created by Mitali Desai on 1/04/2023.
//

import Foundation
import SwiftUI

///   Will return navigation view height 0 and hide
struct HiddenNavigationBar: ViewModifier {
    func body(content: Content) -> some View {
        content
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarHidden(true)
    }
}

extension View {
    func hiddenNavigationBarStyle() -> some View {
        modifier( HiddenNavigationBar() )
    }
}

extension View {
    
    func hasScrollEnabled(_ value: Bool) -> some View {
        self.onAppear {
            UITableView.appearance().isScrollEnabled = value
        }
    }
}

struct kn: UIViewRepresentable {
    @Binding var query: String
    var onSearch: () -> Void
    
    class Coordinator: NSObject, UISearchBarDelegate {
        var onSearch: () -> Void
        
        init(onSearch: @escaping () -> Void) {
            self.onSearch = onSearch
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            DispatchQueue.main.async {
                self.onSearch()
            }
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
    
    func makeUIView(context: Context) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        searchBar.searchBarStyle = .minimal
        searchBar.autocapitalizationType = .none
        searchBar.autocorrectionType = .no
        searchBar.enablesReturnKeyAutomatically = false
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Search movies"
        return searchBar
    }
    
    func updateUIView(_ uiView: UISearchBar, context: Context) {
        uiView.text = query
        context.coordinator.onSearch = search
        search()
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(onSearch: onSearch)
    }
    
    private func search() {
        onSearch()
    }
}
