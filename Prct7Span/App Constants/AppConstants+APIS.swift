//
//  AppAPIs.swift
//  Prct7Span
//
//  Created by Mitali Desai on 01/04/2023.
//

import Foundation

class AppConstants{
    struct AppAPIS{
        
        struct ServerURLs{
            
            static let BASE_URL = "https://api.themoviedb.org/3/"
        
            
            // upload image
            static let API_UPLOADS = "https://image.tmdb.org/t/p/w500"
            
        }
        
        struct APIs{
            static let upcoming = "movie/upcoming"
            static let popular = "movie/popular"
            static let toprated = "movie/top_rated"
            static let fav = "favorite"
           
        }
    }
}
