//
//  RepoMovieDetail.swift
//  Prct7Span
//
//  Created by Mitali Desai on 01/04/23.
//

import Foundation


class RepoMoviewDetail{
    
    let delegate = CustomURLSessionDelegate()
    
    func apimovieDetail(_ id : String,_ completion : @escaping((_ data : MovieDetail?,_ error : String?) -> ())){
        
        var request = URLRequest(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.BASE_URL)movie/\(id)?api_key=c3f030a72294caf3cfbc0675e82aa176&language=en-US")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: .default, delegate: delegate, delegateQueue: nil)
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                completion(nil,error?.localizedDescription ?? "")
                return
            }
            
            do {
                
                let decodeData = try JSONDecoder().decode(MovieDetail.self, from: data)
                completion(decodeData,"")
            }catch (let error){
                completion(nil,error.localizedDescription)
            }
        }
        task.resume()
    }
    func apiFavMovie(_ id : String,_ completion : @escaping((_ status : Bool,_ error : String?) -> ())){
        
        var request = URLRequest(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.BASE_URL)movie/\(id)/\(AppConstants.AppAPIS.APIs.fav)?api_key=c3f030a72294caf3cfbc0675e82aa176&language=en-US")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        
        let session = URLSession(configuration: .default, delegate: delegate, delegateQueue: nil)
        let task = session.dataTask(with: request) { data, response, error in
            guard data != nil else {
                print(String(describing: error))
                completion(false,error?.localizedDescription ?? "")
                return
            }
            
           
            completion(true,"")
            
        }
        task.resume()
    }
}
