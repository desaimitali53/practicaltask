//
//  moviesDetailsView.swift
//  Prct7Span
//
//  Created by Mitali Desai on 1/04/2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct MovieDetailsView: View {
    
    //MARK: Properties
    
    @State private var isExpanded : Bool = false
    @State private var viewModel : MoviewDetailViewModel? = nil
    @State private var movieDetailData : MovieDetail?
    @State private var isFavorite = false
    
    var movieId : String?
    
    var readMore : AttributedString = {
        
        var Str = try! AttributedString(markdown: "....Read More")
        Str.font = Font.system(size: 12)
        Str.foregroundColor = .blue
        
        if let range = Str.range(of: "....") {
            Str[range].foregroundColor = .blue
        }
        
        return Str
    }()
    
    //MARK: Body
    var body: some View {
        
        VStack{
            
            //MARK: Navigation view movie preview
            
            NavigationHeaderView(isTitleShow: true, Title: movieDetailData?.originalTitle,isShowBack : true)
                .frame(height: 45)
            
            ScrollView (showsIndicators: false){
                
                Spacer()
                    .frame(height: 14.67)
                
                Group{
                    ZStack(alignment: .bottomTrailing){
                        WebImage(url: URL(string: AppConstants.AppAPIS.ServerURLs.API_UPLOADS + (movieDetailData?.posterPath ?? "")))
                            .resizable()
                            .frame(height: 200)
                            .aspectRatio(contentMode: .fit)
                        
                        
                        
                        
                        Button(action: {
                            toggleFavorite()
                        }) {
                            Image(systemName: isFavorite ? "heart.fill" : "heart")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .foregroundColor(isFavorite ? .red : .gray)
                                .frame(width: 25, height: 25,alignment: .bottomTrailing)
                            
                            
                        }
                        .padding()
                        
                    }
                }
                
                Spacer()
                    .frame(height: 44)
                
                //MARK: movie description
                
                Group{
                    
                    Text(movieDetailData?.originalTitle ?? "")
                        .modifier(AppFonts(fontName: .medium, size: 18, forgroundColor: .black))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .frame(height: 12)
                        .padding(.leading, 16)
                    
                    Spacer()
                        .frame(height: 12)
                    
                    Text(movieDetailData?.title ?? "")
                        .modifier(AppFonts(fontName: .light, size: 14, forgroundColor: .black))
                        .lineLimit(4)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 16.46)
                        .padding(.trailing, 141.62)
                    
                    Spacer()
                        .frame(height: 12.39)
                    
                    HStack{
                        Text("Release Date: ")
                            .modifier(AppFonts(fontName: .bold, size: 12,forgroundColor: .black))
                        
                        Spacer()
                        
                        Text(movieDetailData?.releaseDate ?? "")
                            .modifier(AppFonts(fontName: .medium, size: 12,forgroundColor: .black))
                            .padding(.trailing,15)
                        
                        
                        .padding()
                        
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, 16.46)
                    
                }
                
                
                Spacer()
                    .frame(height: 28)
                
                
                
                Text("Movie Overview")
                    .modifier(AppFonts(fontName: .medium, size: 18,forgroundColor: .blue))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, 16)
                    .frame(height: 30)
                
                
                
                Text(movieDetailData?.overview ?? "")
                    .lineLimit(isExpanded ? nil : 3)
                    .modifier(AppFonts(fontName: .light, size: 14, forgroundColor: .black))
                    .frame(alignment: .leading)
                    .padding(.leading, 16)
                    .padding(.trailing, 56)
                    .overlay(
                        GeometryReader { proxy in
                            
                            Button(action: {
                                
                                isExpanded.toggle()
                                
                            }) {
                                Text(isExpanded ? "Read Less" : readMore)
                                    .modifier(AppFonts(fontName: .regular, size: 12, forgroundColor: .blue))
                                    .padding(.leading, 8.0)
                                    .background(Color.white)
                                
                            }
                            .frame(width: proxy.size.width, height: proxy.size.height, alignment: .bottomTrailing)
                        }
                    )
                
            }
            Spacer()
        }
        .hiddenNavigationBarStyle()
        .onAppear {
            viewModel = {
                
                return MoviewDetailViewModel {message, status in
                    
                }
            }()
            print(movieId ?? "")
            viewModel?.apiGetMoviewDetail(movieId ?? "") { data in
                
                movieDetailData = data
            }
        }
    }
    func toggleFavorite() {
        
        viewModel?.apiGetMoviewDetail(movieId ?? "", { data in
            isFavorite.toggle()
        })
    }
}


struct moviesDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        
        MovieDetailsView()
        
        
    }
}
