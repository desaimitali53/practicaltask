//
//  MovieDetailViewModel.swift
//  Prct7Span
//
//  Created by Mitali Desai on 01/04/23.
//

import Foundation

class MoviewDetailViewModel{
    
    var fail : BindFail?
    var repo = RepoMoviewDetail()
    
    init(_ FailBlock : @escaping BindFail){
        
        fail = FailBlock
        
    }
    
    
    func apiGetMoviewDetail(_ Id : String,_ Completion : @escaping((_ data : MovieDetail?)->())){
        
        repo.apimovieDetail(Id) { data, error in
            
            if data != nil && error == ""{
                Completion(data)
            }else{
                self.fail?(error ?? "", false)
            }
        }
    }
    func apiFavMovie(_ Id : String,_ Completion : @escaping((_ status : Bool?)->())){
        
        repo.apiFavMovie(Id) { status, error in
            
            if status != false && error == ""{
                Completion(true)
            }else{
                self.fail?(error ?? "", false)
            }
        }
    }
}
