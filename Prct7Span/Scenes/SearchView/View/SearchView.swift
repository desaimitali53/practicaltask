//
//  SearchView.swift
//  Prct7Span
//
//  Created by Mitali Desai on 30/03/23.
//

import SwiftUI

import SwiftUI

struct SearchView: View {
    @State private var query: String = ""
    @State private var filteredMovies: [Result] = []
    var arrCategorymovies : [Result]?
    var movies : [Result] {
        if !query.isEmpty {
            return arrCategorymovies?.filter { movie in
                let titleMatch = movie.title?.localizedCaseInsensitiveContains(query)
                return titleMatch ?? false
            } ?? []
        } else {
            return arrCategorymovies ?? []
        }
    }
    var title : String?
    
    var body: some View {
        VStack {
            NavigationHeaderView(isTitleShow: true, Title: "Movies" ,isShowBack : true)
                .frame(height: 45)
            SearchBar(text: $query)
            .padding()
            List(movies, id: \.id) { movie in
                NavigationLink(destination: MovieDetailsView(movieId : "\(movie.id ?? 0)")) {
                    HStack {
                        AsyncImage(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.API_UPLOADS)\(movie.posterPath ?? "")")) { image in
                            image.resizable()
                                .aspectRatio(contentMode: .fit)
                        } placeholder: {
                            ProgressView()
                        }
                        .frame(width: 60, height: 90)
                        
                        Text(movie.title ?? "")
                    }
                }
            }
            
        }.hiddenNavigationBarStyle()
        
    }
    func search() {
        filteredMovies = movies.filter { movie in
            let titleMatch = movie.title?.localizedCaseInsensitiveContains(query)
            return titleMatch ?? false
        }
    }
}
struct SearchBar: View {
    @Binding var text: String

    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
            TextField("Search Movie", text: $text)
                .textFieldStyle(RoundedBorderTextFieldStyle())
        }
        .padding()
    }
}



struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
