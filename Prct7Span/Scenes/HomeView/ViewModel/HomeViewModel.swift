//
//  HomeViewModel.swift
//  Prct7Span
//
//  Created by Mitali Desai on 26/03/23.
//

import Foundation


class HomeViewModel : ObservableObject {
    
    var fail : BindFail?
    
    private var repo = RepoHome()
    private var upcomingMovies : [UpcomingMovieModel] = []
    private var latestMovies : [UpcomingMovieModel] = []
    private var popularMovies : [UpcomingMovieModel] = []
    var filteredMovies: Observable<[UpcomingMovieModel]> = .init([])
    var homeScreenData : Observable<[UpcomingMovieModel]> = .init([])
    @Published var query: String = ""
    var taskGroup = DispatchGroup()
    
    init(_ failBlock : @escaping BindFail){
        fail = failBlock
        refreshData()
        
        taskGroup.notify(queue: .main) {
            
            var allmovies = [UpcomingMovieModel]()
            
            allmovies += self.upcomingMovies
            allmovies += self.latestMovies
            allmovies += self.popularMovies
            
            self.homeScreenData.value = allmovies
            self.updateFilteredMovies()
        }
    }
    
    func refreshData(){
        
        getUpcomingmovies()
        getPopularmovies()
        getTopratedMovies()
        
    }
    
    private func getUpcomingmovies(){
        
        taskGroup.enter()
        
        repo.apiGetUpcomingMoviesData("upcoming") { data, error in
            
            self.taskGroup.leave()
            
            if data != nil && error == "" {
                self.upcomingMovies = data ?? []
            }else{
                self.fail?(error ?? "", false)
            }
        }
    }
    
    private func getPopularmovies(){
        
        taskGroup.enter()
        
        repo.apiGetPopularMoviesData("popular") { data, error in
            
            self.taskGroup.leave()
            
            if data != nil && error == "" {
                self.popularMovies = data ?? []
            }else{
                self.fail?(error ?? "", false)
            }
        }
    }
    
    private func getTopratedMovies(){
        
        taskGroup.enter()
        
        repo.apiGetTopRatedMoviesData("toprated") { data, error in
            
            self.taskGroup.leave()
            
            if data != nil && error == "" {
                self.latestMovies = data ?? []
            }else{
                self.fail?(error ?? "", false)
            }
        }
    }
    private func updateFilteredMovies() {
        if query.isEmpty {
            filteredMovies.value = homeScreenData.value
        } else {
            filteredMovies.value = homeScreenData.value.filter { $0.results?.map{$0}[0].title?.lowercased().contains(query.lowercased()) ?? false }
        }
    }
    
    func search() {
        updateFilteredMovies()
    }
}
