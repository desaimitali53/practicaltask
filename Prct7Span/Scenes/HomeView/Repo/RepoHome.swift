//
//  RepoHome.swift
//  Prct7Span
//
//  Created by Mitali Desai on 26/03/23.
//

import Foundation

class RepoHome{
    
    
    let delegate = CustomURLSessionDelegate()
    
//    apiGetUpcomingMoviesData
    func apiGetUpcomingMoviesData(_ type : String,_ completion : @escaping((_ data : [UpcomingMovieModel]?,_ error : String?) -> ())){
        
        var request = URLRequest(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.BASE_URL)\(AppConstants.AppAPIS.APIs.upcoming)?api_key=c3f030a72294caf3cfbc0675e82aa176&language=en-US&page=1")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let session = URLSession(configuration: .default, delegate: delegate, delegateQueue: nil)
        let task = session.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
              completion(nil,error?.localizedDescription ?? "")
            return
          }
          
            do {
                
                let decodeData = try JSONDecoder().decode(UpcomingMovieModel.self, from: data)
                completion([decodeData],"")
            }catch (let error){
                completion(nil,error.localizedDescription)
            }
        }
        task.resume()
    }
    
//    apiGetTopRatedMoviesData
    func apiGetTopRatedMoviesData(_ type : String,_ completion : @escaping((_ data : [UpcomingMovieModel]?,_ error : String?) -> ())){
        
        var request = URLRequest(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.BASE_URL)\(AppConstants.AppAPIS.APIs.toprated)?api_key=c3f030a72294caf3cfbc0675e82aa176&language=en-US&page=1")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let session = URLSession(configuration: .default, delegate: delegate, delegateQueue: nil)
        let task = session.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
              completion(nil,error?.localizedDescription ?? "")
            return
          }
          
            do {
                
                let decodeData = try JSONDecoder().decode(UpcomingMovieModel.self, from: data)
                completion([decodeData],"")
            }catch (let error){
                completion(nil,error.localizedDescription)
            }
        }
        task.resume()
    }
    
//    apiGetPopularMoviesData
    func apiGetPopularMoviesData(_ type : String,_ completion : @escaping((_ data : [UpcomingMovieModel]?,_ error : String?) -> ())){
        
        var request = URLRequest(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.BASE_URL)\(AppConstants.AppAPIS.APIs.popular)?api_key=c3f030a72294caf3cfbc0675e82aa176&language=en-US&page=1")!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"

        let session = URLSession(configuration: .default, delegate: delegate, delegateQueue: nil)
        let task = session.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
              completion(nil,error?.localizedDescription ?? "")
            return
          }
          
            do {
                
                let decodeData = try JSONDecoder().decode(UpcomingMovieModel.self, from: data)
                completion([decodeData],"")
            }catch (let error){
                completion(nil,error.localizedDescription)
            }
        }
        task.resume()
    }
}


class CustomURLSessionDelegate: NSObject, URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                let credential = URLCredential(trust: serverTrust)
                completionHandler(.useCredential, credential)
            } else {
                completionHandler(.cancelAuthenticationChallenge, nil)
            }
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
}


