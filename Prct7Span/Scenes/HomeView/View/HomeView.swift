//
//  HomeView.swift
//  Prct7Span
//
//  Created by Mitali Desai on 26/03/23.
//

import SwiftUI


struct HomeView: View {
    
    //MARK: - Properties
    
    @State private var viewModel : HomeViewModel? = nil
    @State private var goTODetailView : Bool = false
    @State private var goToExplorelView : Bool = false
    @State private var deatilOfmovies : Result?
    @State private var arrCategorymovies : [UpcomingMovieModel]?
    @State private var categoryData : UpcomingMovieModel?
  
    
    //MARK: Body
    
    var body: some View {
        VStack{
            NavigationHeaderView(isTitleShow: true, Title: "Home",isShowBack : false)
                .frame(height: 45)
            
            ScrollView (showsIndicators: false){
                VStack {
                    
                    
                    // Vertical view inside horizontal cell
                    Group{
                        
                        ForEach(arrCategorymovies ?? []){ item in
                            
                            MoviesViewCell(movieCategoryItem: item, didTapOnSeeMore: {
                                
                                goToExplorelView = true
                                categoryData = item
                                
                            }, didTapOnCell: { selectedmovie in
                                
                                goTODetailView = true
                                deatilOfmovies = selectedmovie
                            })
                        }
                    }
                }
               
            }
            Spacer()
                .frame(height: 2)
            
            NavigationLink(destination: MovieDetailsView(movieId : "\(deatilOfmovies?.id ?? 0)"),isActive: $goTODetailView){
                EmptyView()
            }
            NavigationLink(destination: SearchView(arrCategorymovies : categoryData?.results ?? [] ,title: ""),isActive: $goToExplorelView){
                EmptyView()
            }
        }.onAppear {
            viewModel = {
                return HomeViewModel { message, status in

                }
            }()
            
            viewModel?.homeScreenData.observe{ homeData in
                
                arrCategorymovies = homeData
                
            }
        }
    }
}


struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}


