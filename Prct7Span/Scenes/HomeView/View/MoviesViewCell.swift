//
//  MoviesViewCell.swift
//  Prct7Span
//
//  Created by Mitali Desai on 26/03/23.
//

import SwiftUI
import SDWebImageSwiftUI

struct MoviesViewCell: View {
    
   var sectionName : String? = ""
   var movieCategoryItem : UpcomingMovieModel?
   var didTapOnSeeMore : emptyCompletion?
   var didTapOnCell : ((Result) -> ())?
  
    var body: some View {
        
        VStack {
            HStack {
                
                Text(sectionName ?? "")
                    .modifier(AppFonts(fontName: .bold, size: 18,forgroundColor: .black))
                    .padding(.leading, 16)
                Spacer()
                
                Text("See more")
                    .modifier(AppFonts(fontName: .medium, size: 14,forgroundColor: .blue))
                    .padding(.trailing, 16)
                    .onTapGesture(perform:  didTapOnSeeMore ?? {} )
                
            }
            
            Spacer()
                .frame(height: 16.49)
            
            ScrollView(.horizontal,showsIndicators : false){
                
                HStack(spacing: 14){
                    ForEach(movieCategoryItem?.results ?? []){ item in
                        
                        VStack (spacing: 0){
                            
                            WebImage(url: URL(string: "\(AppConstants.AppAPIS.ServerURLs.API_UPLOADS)\(item.posterPath ?? "")"))
                                .resizable()
                                .frame(width: 110, height: 160, alignment: .center)
                                .aspectRatio(contentMode: .fill)
                            
                            
                            Spacer()
                                .frame(height: 8.74)
                            Group {
                                
                                Text(item.title ?? "")
                                    .modifier(AppFonts(fontName: .medium, size: 12, forgroundColor: .black))
                                    .multilineTextAlignment(.leading)
                                    .lineLimit(2)
                                    .frame(width:110,alignment: .leading)
                                
                                Spacer()
                                    .frame(height: 8.94)
                                
                                Text(item.originalLanguage ?? "")
                                    .modifier(AppFonts(fontName: .light, size: 12,forgroundColor: .gray))
                                    .lineLimit(2)
                                    .frame(width:110,alignment: .leading)
                               
                            }
                            Spacer()
                        }.onTapGesture {
                            didTapOnCell?(item)
                        }
                    }
                }
                .padding(.leading, 16)
                .padding(.trailing, 16)
            }
        }
        .padding(.top, 15.83)
        .padding(.bottom, 15.83)
    }
}

struct MoviesViewCell_Previews: PreviewProvider {
    static var previews: some View {
        MoviesViewCell()
    }
}


