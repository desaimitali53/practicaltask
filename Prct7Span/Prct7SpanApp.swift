//
//  Prct7SpanApp.swift
//  Prct7Span
//
//  Created by Mitali Desai on 26/03/23.
//

import SwiftUI

@main
struct Prct7SpanApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            NavigationView{
                HomeView()
            }
        }
    }
}
